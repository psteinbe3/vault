from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
import pandas
import sys
from multiprocessing import Pool
from difflib import SequenceMatcher
import concurrent.futures
def get_vault_ticker(company_name,search_result='0'):
    try:
        if company_name.__contains__("LLP"):
            company_name=company_name[:-4]
        original_company_name=company_name
        company_name = company_name.replace(",","%2c")
        company_name = company_name.replace("&","%26")
        company_name = company_name.replace(" ", "+")
        hdr = {'User-Agent': 'Mozilla/5.0'}
        content= urlopen(Request("http://www.vault.com/search-results?q="+company_name,headers=hdr))
        soup = BeautifulSoup(content,'lxml')

        link=soup.find("a", id="ContentPlaceHolderDefault_SectionContent_ProfileRepeater_Title_"+search_result)
        content= urlopen(Request("http://www.vault.com/"+link.get('href'),headers=hdr))
        soup = BeautifulSoup(content,'lxml')
        page_title = soup.find('title', class_="notranslate").get_text().strip()
        page_title = page_title.split("|")
        print(page_title[0])
        list=soup.find("ul", class_="bulletless text statsList")
        list=list.find_all('li')
        if list[1].get_text().__contains__("Stock"):
            if SequenceMatcher(None, page_title[0], original_company_name).ratio() > .7:
                print(list[1].get_text().strip()[14:])
                return list[1].get_text().strip()[14:]
            else:
                return list[1].get_text().strip()[14:]+" ************FLAGGED**********"
        else:
            return 'not_found'
    except Exception as e:
        print(e)
        print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        if str(e).__contains__("WinError"):
            return get_vault_ticker(company_name, '0')
        elif search_result == '0':
            return get_vault_ticker(company_name,'1')
        else:
            return 'not found'

def start_vault_scrape(url):
    hdr = {'User-Agent': 'Mozilla/5.0'}
    content= urlopen(Request(url,headers=hdr))
    df=pandas.DataFrame()
    soup = BeautifulSoup(content,'lxml')
    title=soup.find('title',class_="notranslate").get_text().strip()
    title=title.split("|")
    title=str(title[1])
    i=1
    for link in soup.find_all('h3'):
        company=str(link.find('a').contents[0])
        dic = {'Company': company, 'ticker':get_vault_ticker(company),'rank':i,'category':title}
        df=df.append(pandas.DataFrame(data=[dic.values()],columns=dic.keys()),ignore_index=True)
        i=i+1
    return df
urls = [line.rstrip('\n') for line in open("C:/Users/Yudi/Documents/Vault_lists/Vault_url_list.txt")]
# urls=["http://www.vault.com/company-rankings/banking/vault-banking-50/?pg=5","http://www.vault.com/company-rankings/banking/most-prestigious-banking-companies?pg=5"]
df=pandas.DataFrame()
with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
    res = {executor.submit(start_vault_scrape, url): url for url in urls}

    for future in concurrent.futures.as_completed(res):
        try:
            df = df.append(future.result(), ignore_index=True)
        except Exception as e:
            print(e)
            pass

print(df)
df.to_csv(r'C:\Users\Yudi\Desktop\Vault_results.csv')
# if __name__ == '__main__':
#
#
#
#
#
#
#     urls = [line.rstrip('\n') for line in open("C:/Users/Yudi/Documents/Vault_lists/Vault_url_list.txt")]
#     p=Pool(4)
#     p.map(start_vault_scrape,urls)
